# == BASE INSTALLATION FOR IDEAPAD == #
#part1
printf '\033c'
echo "Welcome to voixe's arch installer script"
echo -
reflector --verbose -l 10 -c GB --sort rate --save /etc/pacman.d/mirrorlist
printf '\033c'
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf
printf '\033c'
pacman -Sy --noconfirm archlinux-keyring
loadkeys uk
timedatectl set-ntp true

# disk
DISK="/dev/mmcblk0"

# disk prep
printf '\033c'
umount -A --recursive /mnt # make sure everything is unmounted before we start
sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment
sgdisk -n 1::+300M --typecode=1:ef00 --change-name=1:'EFIBOOT' ${DISK} # partition 2 (UEFI Boot Partition)
sgdisk -n 2::-0 --typecode=2:8300 --change-name=2:'ROOT' ${DISK} # partition 3 (Root), default start, remaining
partprobe ${DISK} # reread partition table to ensure it is correct

# format
printf '\033c'
mkfs.fat -F 32 "$DISK"p1
mkfs.ext4 "$DISK"p2

# mount
mount "$DISK"p2 /mnt
mkdir /mnt/boot
mount "$DISK"p1 /mnt/boot

printf '\033c'
pacstrap /mnt base base-devel linux linux-firmware intel-ucode
printf '\033c'
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' `basename $0`  > /mnt/base2.sh
chmod +x /mnt/base2.sh
arch-chroot /mnt ./base2.sh
exit

#part2
printf '\033c'
pacman -S --noconfirm --needed sed
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
printf '\033c'
locale-gen
echo "LANG=en_GB.UTF-8" >/etc/locale.conf
echo "KEYMAP=uk" > /etc/vconsole.conf
echo "FONT=ter-i12n" >> /etc/vconsole.conf
hostname="ideapad"
echo $hostname > /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
printf '\033c'
echo root:jay | chpasswd

printf '\033c'
pacman -S --noconfirm --needed networkmanager
pacman -S --noconfirm --needed zsh 
pacman -S --noconfirm --needed dash
pacman -S --noconfirm --needed yadm 
pacman -S --noconfirm --needed git
pacman -S --noconfirm --needed sx
pacman -S --noconfirm --needed opendoas
pacman -S --noconfirm --needed terminus-font
pacman -S --noconfirm --needed xorg
pacman -S --noconfirm --needed xorg-xinit
pacman -S --noconfirm --needed linux-headers

echo 

systemctl enable NetworkManager
systemctl enable fstrim.timer
ln -sfT dash /usr/bin/sh 
echo "%wheel ALL=(ALL) NOPASSWD: ALL " >> /etc/sudoers
useradd -mG wheel -s /bin/zsh jay
echo jay:jay | chpasswd
echo "permit nopass $username" >> /etc/doas.conf
echo "vm.swappiness=10" >> /etc/sysctl.d/99-swappiness.conf

# systemd-boot
printf '\033c'
bootctl install

cat << EOF > /boot/loader/loader.conf
default arch
timeout 10
editor yes
EOF

lsblk
echo "enter your linux partition: 0 or 1?"
read DISK
PARTUUID=$(blkid -s PARTUUID -o value /dev/mmcblk"$DISK"p2)

cat << EOF > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options root=PARTUUID=$PARTUUID rw intel_pstate=no_hwp rootfstype=ext4 nowatchdog
EOF

#cat << EOF > /boot/loader/entries/arch-fallback.conf
#title Arch Linux Fallback
#linux /vmlinuz-linux
#initrd /intel-ucode.img
#initrd /initramfs-linux-fallback.img
#options root=$PARTUUID rw
#EOF
#
#cat << EOF > /boot/loader/entries/arch-terminal.conf
#title Arch Linux Terminal
#linux /vmlinuz-linux
#initrd /intel-ucode.img
#initrd /initramfs-linux.img
#options root=$PARTUUID rw systemd.unit=multi-user.target
#EOF

systemctl enable systemd-boot-update.service

printf '\033c'

path3=/home/jay/base3.sh
sed '1,/^#part3$/d' base2.sh > $path3
chown jay:jay $path3
chmod +x $path3
su -c $path3 -s /bin/sh jay
exit 

#part3
cd /tmp
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si --noconfirm

#sudo sed -i '17s/.//' /etc/paru.conf
#sudo sed -i '35s/.//' /etc/paru.conf
#sudo sed -i '38s/.//' /etc/paru.conf

paru -S --noconfirm zramd
paru -S --noconfirm zsh-fast-syntax-highlighting
paru -S --noconfirm zsh-pure-prompt

sudo systemctl enable zramd

cd $HOME
mkdir -p ~/dl ~/vid ~/music ~/doc ~/code ~/pic/wallhaven ~/anime ~/git

yadm clone https://gitlab.com/voxie12/ideapad.git

cd suckless 
cd dwm
sudo make clean install
cd ..
cd dmenu
sudo make clean install
cd ..
cd slstatus
sudo make clean install
cd ..
cd st
sudo make clean install
cd ..

mkdir -p ~/.cache/zsh
touch ~/.cache/zsh/history

echo "Pre-Installation Finish Reboot now"
echo "Pre-Installation Finish Reboot now"
echo "Pre-Installation Finish Reboot now"

exit
