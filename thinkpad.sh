#!/usr/bin/env bash

#part1
printf '\033c'
echo "Welcome to Arch Linux Magic Script"
setfont -d
reflector -l 10 -c GB --verbose --sort rate --save /etc/pacman.d/mirrorlist
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacman --noconfirm -Sy archlinux-keyring
loadkeys uk
timedatectl set-ntp true

DISK="/dev/nvme0n1"
DISK1="/dev/nvme0n1p1"
DISK2="/dev/nvme0n1p2"

# edit partitions
printf '\033c'
umount -A --recursive /mnt
sgdisk -Z $DISK
sgdisk -a 2048 -o $DISK
sgdisk -n 1::+500M --typecode=1:ef00 $DISK
sgdisk -n 2::-0 --typecode=2:8300 $DISK
partprobe $DISK

# format
printf '\033c'
mkfs.fat -F 32 $DISK1
mkfs.ext4 $DISK2

# mount
mount $DISK2 /mnt
mkdir /mnt/boot
mount $DISK1 /mnt/boot

sed -i 's/^#Para/Para/' /mnt/etc/pacman.conf
printf '\033c'
pacstrap /mnt base base-devel linux linux-firmware intel-ucode
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' thinkpad.sh > /mnt/thinkpad2.sh
chmod +x /mnt/thinkpad2.sh
arch-chroot /mnt ./thinkpad2.sh
exit 

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >> /etc/locale.conf
echo "KEYMAP=uk" >> /etc/vconsole.conf
#echo "FONT=ter-i12n" >> /etc/vconsole.conf
hostname="thinkpad"
echo $hostname >> /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
echo root:jay | chpasswd

# systemd-boot
printf '\033c'
bootctl install

cat << EOF > /boot/loader/loader.conf
default arch
timeout 5
editor yes
EOF

DISK2="/dev/nvme0n1p2"

cat << EOF > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options root=$DISK2 rw
EOF

cat << EOF > /boot/loader/entries/arch-fallback.conf
title Arch Linux Fallback
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux-fallback.img
options root=$DISK2 rw
EOF

cat << EOF > /boot/loader/entries/arch-terminal.conf
title Arch Linux Terminal
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options root=$DISK2 rw systemd.unit=multi-user.target
EOF

systemctl enable systemd-boot-update.service

sed -i 's/^#Para/Para/' /etc/pacman.conf
sed -i 's/^#Col/Col/' /etc/pacman.conf

pacman -S --noconfirm xorg xorg-xinit zsh sxiv xwallpaper \
    networkmanager git dash udiskie qbittorrent youtube-dl pacman-contrib \
    ranger neofetch alacritty opendoas expac exa bat dunst unclutter sxhkd \
    openssh yadm neovim terminus-font wget mpv noto-fonts noto-fonts-cjk \
    noto-fonts-emoji lxappearance neofetch firefox \
    telegram-desktop bitwarden-cli reflector rsync pcmanfm linux-headers \
    speedtest-cli dust duf ripgrep xf86-video-intel lazygit xcompmgr pamixer

# pipewire
pacman -S --noconfirm pipewire pipewire-alsa pipewire-jack pipewire-pulse gst-plugin-pipewire libpulse wireplumber alsa-utils pulsemixer

systemctl enable NetworkManager
systemctl enable fstrim.timer
systemctl enable sshd
ln -sfT dash /usr/bin/sh
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
username=jay
useradd -mG wheel -s /bin/zsh $username
echo $username:jay | chpasswd
echo "permit nopass $username" >> /etc/doas.conf
echo "vm.swappiness=10" >> /etc/sysctl.d/99-swappiness.conf

tp3_path=/home/$username/thinkpad3.sh
sed '1,/^#part3$/d' thinkpad2.sh > $tp3_path
chown $username:$username $tp3_path
chmod +x $tp3_path
su -c $tp3_path -s /bin/sh $username
exit 

#part3
cd /tmp
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si --noconfirm

sudo sed -i '17s/.//' /etc/paru.conf
sudo sed -i '36s/.//' /etc/paru.conf
sudo sed -i '39s/.//' /etc/paru.conf

#paru -S --noconfirm brave-bin
paru -S --noconfirm zsh-fast-syntax-highlighting
paru -S --noconfirm zramd
paru -S --noconfirm nerd-fonts-hack
paru -S --noconfirm nvim-packer-git
#paru -S --noconfirm picom-jonaburg-git 
#paru -S --noconfirm peerflix 
#paru -S --noconfirm librewolf-bin 
#paru -S --noconfirm cava 
paru -S --noconfirm xcursor-breeze 
#paru -S --noconfirm ttyper-git
paru -S --noconfirm fast
paru -S --noconfirm fetchit-git

sudo systemctl enable zramd
systemctl enable --user pipewire-pulse.socket

cd $HOME

yadm clone https://gitlab.com/voxie12/thinkpad.git -f

cd suckless 
cd dwm
sudo make clean install
cd ..
cd st
sudo make clean install
cd ..
cd dmenu
sudo make clean install
cd ..
cd slstatus
sudo make clean install
cd ..
cd slock
sudo make clean install

cd $HOME
mkdir -p ~/dl ~/vid ~/mus ~/doc ~/code ~/anime ~/git ~/scripts

yadm clone https://gitlab.com/voxie12/thinkpad.git

git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1

mkdir -p ~/.cache/zsh
touch ~/.cache/zsh/history

echo "#######################################"
echo "### Finished! umount -a and reboot ####"
echo "#######################################"

exit
