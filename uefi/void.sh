# == void linux installation == #
#part1
lsblk
echo "Enter the Drive: "
read drive
fdisk $drive

lsblk
echo "Enter the Linux Partition: "
read partition 
mkfs.ext4 $partition
mount $partition /mnt
mkdir -p /mnt/boot/efi

lsblk
echo "Enter the efi Partition: "
read efipart
mkfs.fat -F 32 $efipart
mount $efipart /mnt/boot/efi

pacman -Sy --noconfirm --needed wget
wget https://alpha.de.repo.voidlinux.org/live/current/void-x86_64-ROOTFS-20210930.tar.xz
tar xvf void-x86_64-ROOTFS-20210930.tar.xz -C /mnt
cp /etc/resolv.conf /mnt/etc/
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' void.sh > /mnt/void2.sh
chmod +x /mnt/void2.sh
arch-chroot /mnt ./void2.sh
exit

#part2
xbps-install -Syu xbps
xbps-install -yu
xbps-install -y base-system
xbps-remove -y base-voidstrap
echo "Enter the Hostname: "
read hostname
echo $hostname > /etc/hostname
echo 'TIMEZONE="Europe/London"' >> /etc/rc.conf
echo 'KEYMAP="us"' >> /etc/rc.conf
echo "en_GB.UTF UTF-8" > /etc/default/libc-locales
sed -i 's/US/GB/g' /etc/locale.conf
xbps-reconfigure -f glibc-locales
echo "Enter root password: "
passwd

echo "Enter your username:"
read username
useradd -mG wheel $username
passwd $username
echo "%wheel ALL=(ALL) NOPASSWD: ALL " >> /etc/sudoers

xbps-install -y grub grub-x86_64-efi efibootmgr vpm neofetch
grub-install grub-install --target=x86_64-efi --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

xbps-install -y NetworkManager
xbps-reconfigure -fa
echo
echo "---------------------------------------------------"
echo "dont forget to do this after reboot"
echo "ln -s /etc/sv/NetworkManager /var/service/"
echo "ln -s /etc/sv/dbus /var/service/"
echo "---------------------------------------------------"
echo
exit
