# == MY ARCH LINUX AUTO INSTALLER == #
#part1
echo "Welcome to Arch Linux Magic Script"
setfont ter-i12n
reflector -l 10 -c GB --verbose --sort rate --save /etc/pacman.d/mirrorlist
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacman --noconfirm -Sy archlinux-keyring
loadkeys uk
timedatectl set-ntp true

# edit your partition
lsblk
echo "Enter the drive: /dev/____?"
read drive
fdisk /dev/$drive

# mounting your main drive
lsblk
echo "Enter the linux partition: /dev/____?"
read partition
mkfs.ext4 /dev/$partition
mount /dev/$partition /mnt

# mounting your efi partition
lsblk
echo "Enter the efi partition: /dev/____?"
read efipart
mkfs.fat -F 32 /dev/$efipart
mkdir -p /mnt/boot
mount /dev/$efipart /mnt/boot

# mounting your swap partition 
read -p "Did you also create swap partition? [y/n]" answer
if [[ $answer = y ]] ; then
  echo "Enter swap partition: /dev/____?"
  read swap
  mkswap /dev/$swap
  swapon /dev/$swap
fi

pacstrap /mnt base linux linux-firmware
#cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' base.sh > /mnt/base2.sh
chmod +x /mnt/base2.sh
arch-chroot /mnt ./base2.sh
exit

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >> /etc/locale.conf
echo "KEYMAP=uk" >> /etc/vconsole.conf
echo "Hostname: "
read hostname
echo $hostname >> /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
echo "#########################################"
echo "enter your root password: "
passwd

# network
pacman -S --noconfirm networkmanager
systemctl enable NetworkManager

# systemd-boot
bootctl install

cat << EOF > /boot/loader/loader.conf
default arch
timeout 5
editor no
EOF

echo "#########################################"
lsblk
echo "#########################################"
echo "Enter the linux partition: /dev/____?"
echo "#########################################"
read partition

cat << EOF > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=/dev/$partition rw 
EOF

cat << EOF > /boot/loader/entries/arch-fallback.conf
title Arch Linux Fallback
linux /vmlinuz-linux
initrd /initramfs-linux-fallback.img
options root=/dev/$partition rw 
EOF

cat << EOF > /boot/loader/entries/arch-terminal.conf
title Arch Linux Terminal
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=/dev/$partition rw systemd.unit=multi-user.target
EOF

systemctl enable systemd-boot-update.service

echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "Enter Username: "
read username
useradd -m -g users -G audio,video,network,wheel,storage,rfkill -s /bin/bash jay
passwd $username

echo "umount -R /mnt and reboot"
exit

