# == SIMPLE SUCKLESS INSTALLATION == #
#part1
printf '\033c'
echo "Welcome to voixe's arch installer script"
reflector --verbose --age 6 --latest 20 --fastest 20 --threads 20 --sort rate --protocol https --save /etc/pacman.d/mirrorlist
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf
pacman -Sy --noconfirm archlinux-keyring
timedatectl set-ntp true
lsblk
echo "Enter the Drive: "
read drive
fdisk $drive
lsblk
echo "Enter the Linux Partition: "
read partition
mkfs.ext4 $partition
read -p "Did you also create efi partition? [y/n]" answer
if [[ $answer = y ]] ; then
  lsblk
  echo "Enter EFI partition: "
  read efipartition
  mkfs.vfat -F 32 $efipartition
fi
mount $partition /mnt
pacstrap /mnt base linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' `basename $0`  > /mnt/simple-suckless2.sh
chmod +x /mnt/simple-suckless2.sh
arch-chroot /mnt ./simple-suckless2.sh
exit

#part2
printf '\033c'
pacman -S --noconfirm sed
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >/etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
echo "Enter the Hostname: "
read hostname
echo $hostname > /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
passwd
pacman --noconfirm -S grub efibootmgr os-prober
lsblk
echo "Enter EFI partition: " 
read efipartition
mkdir /boot/efi
mount $efipartition /boot/efi 
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
sed -i 's/quiet/pci=noaer/g' /etc/default/grub
#sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

pacman -S --noconfirm xorg xorg-xinit firefox git networkmanager reflector rsync dmenu base-devel
systemctl enable NetworkManager
echo "%wheel ALL=(ALL) NOPASSWD: ALL " >> /etc/sudoers
echo "Enter Username: "
read username 
useradd -mG wheel $username
passwd $username
echo "Pre-Installation Finish Reboot now"
exit

#part3
#cd $HOME
#repos=( "dwm" "st" "slstatus" )
#for repo in ${repos[@]}
#do
#    git clone git://git.suckless.org/$repo
#    cd $repo;sudo make clean install;cd ..
#done
#echo "exec dwm" >> ~/.xinitrc
#cat > ~/.xinitrc << EOF
#slstatus &
#exec dwm
#EOF
#echo "startx" >> ~/.bash_profile
#EOF
#printf "\e[1;32mDone! you can now reboot.\e[0m\n"
#exit
