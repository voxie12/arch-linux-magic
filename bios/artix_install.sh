# == MY Artix LINUX AUTO INSTALLER == #
# == for only runit!!!  == #
#part1
echo "Welcome to Artix Linux Magic Script"
setfont ter-i12n
#reflector -l 10 -c GB --verbose --sort rate --save /etc/pacman.d/mirrorlist
sed -i 's/^#Para/Para/' /etc/pacman.conf
#pacman --noconfirm -Sy archlinux-keyring
loadkeys us
#timedatectl set-ntp true
lsblk
echo "Enter the drive: "
read drive
fdisk $drive 
lsblk
echo "Enter the linux partition: "
read partition
mkfs.ext4 $partition 
mount $partition /mnt 
basestrap /mnt base base-devel runit elogind-runit linux linux-firmware intel-ucode
#cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
fstabgen -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' artix_install.sh > /mnt/artix_install2.sh
chmod +x /mnt/artix_install2.sh
arch-chroot /mnt ./artix_install2.sh
exit 

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG='en_GB.UTF-8'" >> /etc/locale.conf
echo "LC_COLLATE='C'" >> /etc/locale.conf
echo "KEYMAP=uk" >> /etc/vconsole.conf
echo "FONT=ter-i12n" >> /etc/vconsole.conf
echo "Hostname: "
read hostname
echo $hostname >> /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
echo root:jay | chpasswd
pacman --noconfirm -S grub 
lsblk
echo "Enter grub drive: "
read gdrive
grub-install --recheck $gdrive
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
sed -i 's/^#Para/Para/' /etc/pacman.conf
sed -i 's/^#Col/Col/' /etc/pacman.conf
pacman -S dash zsh
ln -sfT dash /usr/bin/sh
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "Enter Username: "
read username
useradd -m -G wheel -s /bin/zsh $username
passwd $username
echo $username:jay | chpasswd
pacman -S opendoas
echo "permit nopass $username" >> /etc/doas.conf

pacman -S networkmanager networkmanager-runnit

pacman -S artix-archlinux-support

echo "[extra]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf


echo "[community]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf

pacman-key --populate archlinux

sudo pacman -S reflector rsync

reflector -l 10 -c GB --verbose --sort rate --save /etc/pacman.d/mirrorlist-arch

ai3_path=/home/$username/arch_install3.sh
sed '1,/^#part3$/d' arch_install2.sh > $ai3_path
chown $username:$username $ai3_path
chmod +x $ai3_path
su -c $ai3_path -s /bin/sh $username
exit 

#part3
cd /tmp
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si --noconfirm

sudo sed -i '17s/.//' /etc/paru.conf
sudo sed -i '34s/.//' /etc/paru.conf
sudo sed -i '37s/.//' /etc/paru.conf

paru -S --noconfirm xorg xwallpaper sxiv alacritty picom sxhkd yadm

paru -S --noconfirm brave-bin nerd-fonts-mononoki nerd-fonts-hack nvim-packer-git
zsh-fast-syntax-highlighting 

cd $HOME

git clone https://gitlab.com/voxie12/suckless.git
cd suckless 
cd dwm
sudo make clean install
cd ..
cd dmenu
sudo make clean install
cd ..
cd slstatus
sudo make clean install
cd ..
cd slock
sudo make clean install

cd $HOME
mkdir -p ~/dl ~/vid ~/music ~/doc ~/code ~/pic/wallhaven ~/anime ~/git

yadm clone https://gitlab.com/voxie12/dotfiles.git

mkdir -p ~/.cache/zsh
touch ~/.cache/zsh/history

echo "#######################################"
echo "### Finished! umount -a and reboot ####"
echo "#######################################"

exit
