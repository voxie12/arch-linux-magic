# == MY ARCH LINUX AUTO INSTALLER == #
#part1
echo "Welcome to Arch Linux Magic Script"
reflector -l 10 -c GB --verbose --sort rate --save /etc/pacman.d/mirrorlist
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacman --noconfirm -Sy archlinux-keyring
loadkeys uk
timedatectl set-ntp true
lsblk
echo "##########################################"
echo "Enter the drive: "
echo "##########################################"
read drive
fdisk $drive 
lsblk
echo "##########################################"
echo "Enter the linux partition: "
echo "##########################################"
read partition
mkfs.ext4 $partition 
mount $partition /mnt 
pacstrap /mnt base linux linux-firmware intel-ucode
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' arch_install.sh > /mnt/arch_install2.sh
chmod +x /mnt/arch_install2.sh
arch-chroot /mnt ./arch_install2.sh
exit 

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >> /etc/locale.conf
echo "KEYMAP=uk" >> /etc/vconsole.conf
echo "FONT=ter-i12n" >> /etc/vconsole.conf
echo "##########################################"
echo "##########################################"
read hostname
echo $hostname >> /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
echo root:jay | chpasswd
pacman --noconfirm -S grub 
lsblk
echo "Enter grub drive: "
read gdrive
grub-install --target=i386-pc $gdrive
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
sed -i 's/^#Para/Para/' /etc/pacman.conf
sed -i 's/^#Col/Col/' /etc/pacman.conf

pacman -S --noconfirm networkmanager reflector rsync linux-headers

systemctl enable NetworkManager
systemctl enable fstrim.timer
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "Enter Username: "
read username
useradd -mG wheel $username
passwd $username
echo $username:jay | chpasswd
echo "vm.swappiness=10" >> /etc/sysctl.d/99-swappiness.conf

exit 
